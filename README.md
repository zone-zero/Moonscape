# Moonscape
## Basic information
A client for the Microsoft Notification Protocol (also known as **Mobile Status Notification Protocol**) for the Android platform, using the Material design language to create a minimalist client for the discontinued service. This project is designed to be able to connect to any servers using the protocol, designed with the Escargot server in mind.

**Notice: the program is unable to sign into the Escargot server but is able to connect. This project has been posted for testing purposes only.**

## Prerequisites
### Installation
To install on your Android device, you need Android "Marshmallow" (version 6.0) with a working Internet connection to connect to the Messenger Service ("Escargot").

## Project build

| Android version   | API level | Tested on master branch | Tested on 1.0 branch |
|-------------------|-----------|-------------------------|----------------------|
| Lollipop 5.0*     | API 21 |     |     |
| Lollipop 5.0.1    |        |     |     |
| Lollipop 5.0.2    |        |     |     |
| Lollipop 5.1*     | API 22 |     |     |
| Lollipop 5.1.1    |        |     |     |
| Marshmallow 6.0   | API 23 | Yes | No  |
| Marshmallow 6.0.1 |        | No  | No  |
| Nougat 7.0        | API 24 | No  | No  |
| Nougat 7.1        | API 25 | No  | No  |
| Nougat 7.1.1      |        | No  | No  |
| Nougat 7.1.2      |        | No  | No  |
| Oreo 8.0          | API 26 | No  | No  |
| Oreo 8.1          | API 27 | No  | No  |

* Lollipop is not tested and cannot install Moonscape. It is listed only for backwards compatbility purposes if implemented in the future with a new branch.

## Legal notices
The following products are not developed or maintained, nor claimed by any contributors to this project:
* The Microsoft Notification Protocol is a protocol designed by Microsoft.
* MSN Messenger and Windows Live Messenger are products designed by Microsoft.
* The Material Design language is designed by Google.
* All API libraries are included in the Android Studio SDK.

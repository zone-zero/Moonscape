package com.outlook.paragonazure.moonscape.msn;

import com.outlook.paragonazure.moonscape.msn.protocol.MSNPVersion;
import com.outlook.paragonazure.moonscape.msn.status.ResponseLogin;

/**
 * Created by paragonazure on 24/12/2017.
 */

public interface MsnClient {

    /**
     * Connects to the MSN Messenger Service and then returns if
     * the connection to the service was successful.
     * @return A boolean value that represents if the connection
     * was successful.
     */
    boolean connect();

    /**
     * Disconnects from the MSN Messenger Service and then
     * returns if the disconnection completed normally.
     * @return A boolean that represents if the disconnection
     * operated normally.
     */
    boolean disconnect();

    /**
     * Gets and returns the version of MSNP used.
     * @return The version of MSNP used to connect.
     */
    MSNPVersion getMSNPVersion();

    /**
     * Gets and returns the server URL used to connect to
     * the MSN Messenger service.
     * @return The server URL of the MSN Messenger service.
     */
    String getServerUrl();

    /**
     * Gets and returns the SOAP URL used to authenticate
     * with the MSN Messenger Service.
     * @return The SOAP URL of the authentication server.
     */
    String getSoapUrl();

    /**
     * Attempts to log in to the MSN Messenger servers and then
     * returns a value the represents if the log in was
     * successful.
     * @param account The Windows Live ID for logging in.
     * @param password The password used to logging in.
     * @return A boolean that represents if logging in was
     * successful.
     */
    ResponseLogin login(String account, String password);
    // TODO Replace value with new functionality (enum)

    /**
     *
     * @return
     */
    String read();

    void setMSNPVersion(MSNPVersion version);
    void setServerUrl(String address);
    void setSoapUrl(String address);

}

package com.outlook.paragonazure.moonscape.msn.status;

/**
 * Created by paragonazure on 24/12/2017.
 */

public enum ResponseLogin {
    LOGIN_SUCCESSFUL,
    LOGIN_FAILED_NOT_CONNECTED,
    LOGIN_FAILED_SERVICE_UNAVAILABLE,
    LOGIN_FAILED_CREDENTIALS_INCORRECT
}

package com.outlook.paragonazure.moonscape.msn;

/**
 * Created by paragonazure on 24/12/2017.
 */

public interface MSNResponder {

    void handle(MsnCommand command);

}

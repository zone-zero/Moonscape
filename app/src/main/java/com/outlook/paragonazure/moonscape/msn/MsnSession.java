package com.outlook.paragonazure.moonscape.msn;

import com.outlook.paragonazure.moonscape.msn.protocol.MSNClient;
import com.outlook.paragonazure.moonscape.msn.protocol.MSNPVersion;

/**
 * Created by paragonazure on 24/12/2017.
 */

public interface MsnSession {

    String getLanguage();
    String getOperatingSystem();
    String getOperatingSystemVersion();
    MSNPVersion getMSNPVersion();
    MSNClient getMessengerClient();
    String getMessengerVersion();

}

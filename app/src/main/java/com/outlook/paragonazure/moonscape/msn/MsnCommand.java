package com.outlook.paragonazure.moonscape.msn;

import android.text.TextUtils;

/**
 * Created by paragonazure on 24/12/2017.
 */

public final class MsnCommand {

    private String[] arguments;
    private String command;
    private String content;

    public MsnCommand(String command, String... args) {
        this.command = command;
        this.arguments = args;
        this.content = command + " " + TextUtils.join(" ", args);
    }

    public String[] getArguments() {
        return arguments;
    }

    public String getCommand() {
        return command;
    }

    public String getContent() {
        return content;
    }
}

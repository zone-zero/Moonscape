package com.outlook.paragonazure.moonscape.msn.impl;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by paragonazure on 24/12/2017.
 */

public class MessengerClientTask extends AsyncTask<String, String, MessengerClient> {

    public boolean check = false;
    private MessengerClient client;
    private static String TAG = "MessengerClientTask";

    public void setClient(MessengerClient client) {
        this.client = client;
    }

    public MessengerClient getClient() {
        return client;
    }

    @Override
    protected MessengerClient doInBackground(String... strings) {
        if (client != null) {
            client.connect();

            if (client.getIsConnected()) {
                if (!check) {
                    Log.i(TAG, "Client connected.");
                    check = true;
                }
            } else {
                if (check) {
                    Log.i(TAG, "Client disconnected.");
                    check = false;
                }
            }

            return client;
        } else return null;
    }
}
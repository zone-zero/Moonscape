package com.outlook.paragonazure.moonscape.msn.net;

/**
 * Created by paragonazure on 24/12/2017.
 */

import android.util.Log;

import com.outlook.paragonazure.moonscape.msn.impl.MessengerClient;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 */
public class MessengerAuth {

    /**
     * Phase 1 ** USR command
     *
     * Client: USR [TrID] SSO I [Email]
     * Server: USR [TrID] SSO S [Policy] [Base64 Nonce]
     * Client: USR [TrID] SSO S [Ticket] [Base64 Response]
     * Server: USR [TrID] OK [Email] [IfVerified]
     */

    /**
     * The tag used in debugging and Logcat.
     */
    private final static String TAG = "MessengerAuth";

    private MessengerClient client;
    private String server;

    public MessengerAuth(MessengerClient client) {
        this.client = client;
        this.server = client.getSoapUrl();
        Log.v(TAG, "SoapUrl == " + server);

        this.server = server;
        Log.i(TAG, "Initializing authentication...");

        SOAPRequest request = new SOAPRequest(client.getSoapUrl(), client.getTicket());
        Document response = request.getResponse();

        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(response);
            transformer.transform(source, result);

            String ticketAsString = sw.toString();
            Log.v(TAG, ticketAsString);

        } catch (Exception ex) {

        }

        String secret;
        String ticket;

        Element body = response.getElementById("S:Body");
        Element securityTokenResponseCollection = (Element)body.getChildNodes().item(0);
        Log.v(TAG, securityTokenResponseCollection.getTextContent());
        Element securityTokenResponse = (Element)body.getChildNodes().item(1);
        Log.v(TAG, securityTokenResponse.getTextContent());

    }
}

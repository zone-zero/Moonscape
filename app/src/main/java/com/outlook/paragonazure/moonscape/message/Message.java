package com.outlook.paragonazure.moonscape.message;

import java.util.Date;

/**
 * Created by paragonazure on 24/12/2017.
 */

public final class Message {

    public Date date;
    public String message;
    public Contact sender;

}

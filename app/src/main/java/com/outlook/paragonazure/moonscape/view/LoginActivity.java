package com.outlook.paragonazure.moonscape.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.outlook.paragonazure.moonscape.R;
import com.outlook.paragonazure.moonscape.msn.MsnCommand;
import com.outlook.paragonazure.moonscape.msn.impl.MessengerClient;
import com.outlook.paragonazure.moonscape.msn.impl.MessengerClientTask;
import com.outlook.paragonazure.moonscape.msn.impl.MessengerResponder;
import com.outlook.paragonazure.moonscape.msn.status.ResponseLogin;

/**
 * Created by paragonazure on 24/12/2017.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private boolean connected;
    private MessengerClientTask msct;
    private MessengerClient client;
    private MessengerResponder responder;

    LinearLayout linearLayout;
    Button loginButton;
    EditText passwordText;
    EditText usernameText;
    TextView signUpView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        usernameText = (EditText)findViewById(R.id.input_email);
        passwordText = (EditText)findViewById(R.id.input_password);
        loginButton  = (Button)findViewById(R.id.button_login);
        linearLayout = (LinearLayout)findViewById(R.id.layout_linear_activity_login);
        signUpView   = (TextView)findViewById(R.id.text_signup);

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }

        });
        signUpView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://escargot.log1p.xyz/#create-account");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        msct = new MessengerClientTask();
        msct.setClient(new MessengerClient(this) {
            @Override
            public void createSnackbar(String message) {
                LoginActivity.this.showSnackbar(message);
            }

            @Override
            public String handleRead(MsnCommand command) {
                return handle(command);
            }
        });
        responder = new MessengerResponder(msct.getClient());
        msct.execute("");
        client = msct.getClient();
    }

    public void login() {
        if (client.getIsConnected()) {
            ResponseLogin response = client.login(usernameText.getText().toString(), passwordText.getText().toString());
            if (response == ResponseLogin.LOGIN_SUCCESSFUL) {
                onLoginSuccess();
            } else {
                onLoginFailed(response);
            }
        }
    }

    public String handle(MsnCommand command) {
        responder.handle(command);
        return "";
    }

    public void onLoginSuccess() {

    }

    public void onLoginFailed(ResponseLogin response) {
        showSnackbar(getResources().getString(R.string.status_login_failed) + ": " + response.name());
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(linearLayout, message, Snackbar.LENGTH_LONG).setDuration(5000);
        snackbar.show();
    }

}

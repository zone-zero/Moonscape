package com.outlook.paragonazure.moonscape.msn.impl;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.LinearLayout;

import com.outlook.paragonazure.moonscape.R;
import com.outlook.paragonazure.moonscape.msn.MsnClient;
import com.outlook.paragonazure.moonscape.msn.MsnCommand;
import com.outlook.paragonazure.moonscape.msn.protocol.MSNPCommand;
import com.outlook.paragonazure.moonscape.msn.protocol.MSNPVersion;
import com.outlook.paragonazure.moonscape.msn.status.ResponseLogin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by paragonazure on 24/12/2017.
 */

public abstract class MessengerClient implements MsnClient {

    private static final long INTERVAL_KEEP_ALIVE     = 1000 * 60 * 4;
    private static final long INTERVAL_INITIAL_RETRY  = 1000 * 10;
    private static final long INTERVAL_MAXIMUM_RETRY  = 1000 * 60 * 2;
    private static String TAG = "MessengerClient";

    private String account;
    private BufferedReader input;
    private boolean isConnected = false;
    private LinearLayout parentLayout;
    private PrintWriter output;
    private String password;
    private Activity parent;
    private int port = 1863;
    private boolean sentOne = false;
    private boolean serviceUnavailable = false;
    private String serverUrl = "m1.escargot.log1p.xyz";
    private MessengerSession session = new MessengerSession();
    private Socket socket;
    private String soapUrl;
    private String ticket;
    private int transaction = 0;
    private Map<Integer, MsnCommand> transactionsReceived = new HashMap<Integer, MsnCommand>();
    private Map<Integer, MsnCommand> transactionsSent = new HashMap<Integer, MsnCommand>();
    private MSNPVersion version;

    public MessengerClient(Activity parent) {
        this.parent = parent;
        this.version = MSNPVersion.MSNP15;
    }

    @Override
    public boolean connect() {
        try {
            socket = new Socket(serverUrl, port);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

            if (!socket.isConnected()) {
                showSnackbar(parent.getResources().getString(R.string.login_connect_failed));
                serviceUnavailable = true;
                return false;
            }

            showSnackbar(parent.getResources().getString(R.string.login_connect_success));

            // TODO Implement external "sendversion"
            send(MSNPCommand.VER, getMSNPVersion().name() + " CVR0");
            send(MSNPCommand.CVR, "0x0409 win 6.10 ix386 MSNMSGR 8.5.1302");

            while (socket.isConnected()) {
                if (!isConnected) isConnected = true;
                String read = read();
                if (read.equalsIgnoreCase("OUT")) {
                    output.close();
                    input.close();
                    socket.close();
                }
            }

            return false;
        } catch (Exception ex) {
            showSnackbar(ex.getMessage());
            ex.printStackTrace();
        } finally {
            parent.getResources().getString(R.string.status_disconnected);
            isConnected = false;
        }
        return false;
    }

    @Override
    public boolean disconnect() {
        try {
            output.close();
            input.close();
            socket.close();
        } catch (Exception ex) {
            showSnackbar(ex.getMessage());
            ex.printStackTrace();
            Log.e(TAG, "Could not disconnect normally from MSN Messenger: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public String getAccount() { return account; }

    public boolean getIsConnected() {
        if (socket != null) return socket.isConnected();
        else return false;
    }

    @Override
    public MSNPVersion getMSNPVersion() {
        return version;
    }

    public String getPassword() { return password; }

    @Override
    public String getServerUrl() {
        return null;
    }

    @Override
    public String getSoapUrl() {
        return soapUrl;
    }

    public String getTicket() {
        return ticket;
    }

    @Override
    public ResponseLogin login(String account, String password) {
        try {
            int transaction = send(MSNPCommand.USR, "SSO", "I", account);
            this.account = account;
            this.password = password;
            return ResponseLogin.LOGIN_SUCCESSFUL;
        } catch (Exception ex) {
            showSnackbar("An error occurred signing in: " + ex.getMessage());
            ex.printStackTrace();
            return ResponseLogin.LOGIN_FAILED_SERVICE_UNAVAILABLE;
        }
    }

    @Override
    public String read() {
        if (socket.isConnected()) {
            try {
                String read = input.readLine();
                Log.i(TAG,"Received from MSN: " + read);

                String[] readSplit = read.split(" ");
                String[] args = new String[readSplit.length - 1];
                System.arraycopy(readSplit,1, args, 0, readSplit.length - 1);

                MsnCommand response = new MsnCommand(readSplit[0], readSplit);
                handleRead(response);
                transactionsReceived.put(transaction, response);

                return read;
            } catch (IOException ex) {
                showSnackbar(ex.getMessage());
                Log.e(TAG, "An error occurred trying to read from the socket.");
                return "";
            }
        } else {
            showSnackbar(parent.getResources().getString(R.string.error_not_connected));
            return "DISCONNECTED";
        }
    }

    public int send(MSNPCommand command, String... args) {
        if (socket.isConnected() || !sentOne) {
            try {
                transaction += 1;
                String out = command.name() + " " + Integer.toString(transaction) + " " + TextUtils.join(" ", args);
                Log.i(TAG, "Sending to MSN: " + out);

                output.println(out);
                output.flush();

                MsnCommand request = new MsnCommand(command.name(), args);
                transactionsSent.put(transaction, request);

                sentOne = true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            showSnackbar(parent.getResources().getString(R.string.error_not_connected));
        }

        return transaction;
    }

    @Override
    public void setMSNPVersion(MSNPVersion version) {
        this.version = version;
    }

    public void setParentView(LinearLayout view) {
        parentLayout = view;
    }

    @Override
    public void setServerUrl(String address) {
        // TODO Implement setServerUrl;
    }

    @Override
    public void setSoapUrl(String address) {
        // TODO Implement setSoapUrl;
        soapUrl = address;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    /**
     * Shows a {@link android.support.design.widget.Snackbar Snackbar} in the
     * activity that manages the {@link MessengerClient}.
     * @param message The message that will be shown in the Snackbar.
     */
    private void showSnackbar(String message) {
        try {
            Log.d(TAG, message);
            createSnackbar(message);
        } catch (Exception ex) {
            Log.e(TAG, "Could not write to Snackbar: " + ex.getMessage());
        }
    }

    public abstract void createSnackbar(String message);
    public abstract String handleRead(MsnCommand command);
}

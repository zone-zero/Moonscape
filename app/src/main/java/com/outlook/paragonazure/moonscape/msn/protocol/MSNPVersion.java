package com.outlook.paragonazure.moonscape.msn.protocol;

/**
 * Created by paragonazure on 24/12/2017.
 */

public enum MSNPVersion {
    /**
     * Represents the earliest available version of MSNP
     */
    MSNP7,

    /**
     * Represents the version of MSNP used in MSN 5.0
     */
    MSNP8,

    /**
     * Represents the version of MSNP used in MSN 6.0
     */
    MSNP9,

    /**
     * Represents the version of MSNP used in MSN 6.1
     */
    MSNP10,

    /**
     * Represents the version of MSNP used in MSN 7.0
     */
    MSNP11,

    /**
     * Represents the version of MSNP used in MSN 7.5
     */
    MSNP12,

    /**
     * Represents the version of MSNP used in Messenger 8.0
     */
    MSNP13,

    /**
     * Represents the version of MSNP used in Messenger 8.0
     */
    MSNP14,

    /**
     * Represents the version of MSNP used in Messenger 8.5
     */
    MSNP15,

    /**
     * Represents the version of MSNP used in Messenger 8.0.1
     */
    MSNP16,

    /**
     * Represents the fallback version of modern Messenger
     */
    MSNP17,

    /**
     * Represents the version of MSNP used in Messenger 14.0
     */
    MSNP18,

    /**
     * Represents the fallback version of late Messenger
     */
    MSNP19,

    /**
     * Represents another fallback version of late Messenger
     */
    MSNP20,

    /**
     * Represents the version of MSNP used in Messenger 15.0
     */
    MSNP21,

    /**
     * Represents the version of MSNP used in Messenger 16.4
     */
    MSNP22,

    /**
     * Represents the version of MSNP used in Skype
     */
    MSNP24,
}

package com.outlook.paragonazure.moonscape.msn.protocol;

/**
 * Created by paragonazure on 24/12/2017.
 */

public enum MSNClient {

    /**
     * Represents MSN Messenger and Windows Live Messenger
     */
    MSNMSGR

}

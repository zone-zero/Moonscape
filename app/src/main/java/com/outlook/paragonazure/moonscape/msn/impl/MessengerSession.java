package com.outlook.paragonazure.moonscape.msn.impl;

import android.util.Log;

import com.outlook.paragonazure.moonscape.msn.MsnSession;
import com.outlook.paragonazure.moonscape.msn.protocol.MSNClient;
import com.outlook.paragonazure.moonscape.msn.protocol.MSNPVersion;

/**
 * Created by paragonazure on 24/12/2017.
 */

public class MessengerSession implements MsnSession {

    private String language = "0x0409";
    private final static String TAG = "MessengerSession";

    public MessengerSession() {
        getLanguage();
        getOperatingSystem();
        getOperatingSystemVersion();
        getMSNPVersion();
        getMessengerClient();
        getMessengerVersion();
    }

    @Override
    public String getLanguage() {
        Log.i(TAG, "messenger.language == " + language);
        return "0x0409";
    }

    @Override
    public String getOperatingSystem() {
        String name = System.getProperty("os.name");
        Log.i(TAG, "os.name == " + name);
        return name.substring(0, 3).toLowerCase();
    }

    @Override
    public String getOperatingSystemVersion() {
        String version = System.getProperty("os.version");
        Log.i(TAG, "os.version == " + version);
        return version;
    }

    @Override
    public MSNPVersion getMSNPVersion() {
        MSNPVersion version = MSNPVersion.MSNP15;
        Log.i(TAG, "protocol.version == " + version.name());
        return version;
    }

    @Override
    public MSNClient getMessengerClient() {
        MSNClient client = MSNClient.MSNMSGR;
        Log.i(TAG, "messenger.client == " + client.name());
        return client;
    }

    @Override
    public String getMessengerVersion() {
        String version = "8.5.1302";
        Log.i(TAG, "messenger.version == " + version);
        return version;
    }
}

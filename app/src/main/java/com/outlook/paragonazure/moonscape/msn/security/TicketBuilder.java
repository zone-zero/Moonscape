package com.outlook.paragonazure.moonscape.msn.security;

import android.net.LocalSocketAddress;
import android.util.Log;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Contains methods and functions used to create an authentication
 * ticket for the MSNP servers.
 */
public class TicketBuilder {

    private final static String TAG = "TicketBuilder";
    private String xml;

    /**
     * Initializes the {@link TicketBuilder} with a policy and nonce for
     * the creation of the XML.
     * @param policy The policy used to manage connecting to the server.
     * @param nonce The nonce used to create an encrypted ticket.
     * @param account The account that the ticket will represent.
     * @param password The password that the account uses to authenticate.
     */
    public TicketBuilder(String policy, String nonce, String account, String password) {
        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2003/06/secext\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2002/12/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\" xmlns:wssc=\"http://schemas.xmlsoap.org/ws/2004/04/sc\" xmlns:wst=\"http://schemas.xmlsoap.org/ws/2004/04/trust\"><Header>";
        xml += "<ps:AuthInfo xmlns:ps=\"http://schemas.microsoft.com/Passport/SoapServices/PPCRL\" Id=\"PPAuthInfo\">";
        xml += "<ps:HostingApp>{7108E71A-9926-4FCB-BCC9-9A9D3F32E423}</ps:HostingApp>";
        xml += "<ps:BinaryVersion>4</ps:BinaryVersion>";
        xml += "<ps:UIVersion>1</ps:UIVersion>";
        xml += "<ps:Cookies></ps:Cookies>";
        xml += "<ps:RequestParams>AQAAAAIAAABsYwQAAAAxMDMz</ps:RequestParams>";
        xml += "</ps:AuthInfo>";
        xml += "<wsse:Security><wsse:UsernameToken Id=\"user\">";
        xml += "<wsse:Username>" + account + "</wsse:Username>";
        xml += "<wsse:Password>" + password + "</wsse:Password>";
        xml += "</wsse:UsernameToken></wsse:Security></Header><Body>";
        xml += "<ps:RequestMultipleSecurityTokens xmlns:ps=\"http://schemas.microsoft.com/Passport/SoapServices/PPCRL\" Id=\"RSTS\">";
        xml += "<wst:RequestSecurityToken Id=\"RST0\">";
        xml += "<wst:RequestType>http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue</wst:RequestType>";
        xml += "<wsp:AppliesTo><wsa:EndpointReference><wsa:Address>http://Passport.NET/tb";
        xml += "</wsa:Address></wsa:EndpointReference></wsp:AppliesTo></wst:RequestSecurityToken>";
        xml += "<wst:RequestSecurityToken Id=\"RST1\">";
        xml += "<wst:RequestType>http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue</wst:RequestType><wsp:AppliesTo><wsa:EndpointReference>";
        xml += "<wsa:Address>messengerclear.live.com</wsa:Address></wsa:EndpointReference></wsp:AppliesTo>";
        xml += "<wsse:PolicyReference URI=\"" + policy + "\"></wsse:PolicyReference></wst:RequestSecurityToken>";
        xml += "<wst:RequestSecurityToken Id=\"RST2\">";
        xml += "<wst:RequestType>http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue</wst:RequestType>";
        xml += "<wsp:AppliesTo>";
        xml += "<wsa:EndpointReference>";
        xml += "<wsa:Address>contacts.msn.com</wsa:Address>";
        xml += "</wsa:EndpointReference>";
        xml += "</wsp:AppliesTo>";
        xml += "<wsse:PolicyReference URI=\"MBI\">";
        xml += "</wsse:PolicyReference>";
        xml += "</wst:RequestSecurityToken>";
        xml += "</ps:RequestMultipleSecurityTokens></Body></Envelope>";

        // TODO Implement Xml builder (improves upgrading)
        /**try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document ticket = builder.newDocument();
            Element envelope = ticket.createElement("Envelope");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", "http://schemas.xmlsoap.org/soap/envelope/");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wsse","http://schemas.xmlsoap.org/ws/2003/06/secext");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xlmns:saml","urn:oasis:names:tc:SAML:1.0:assertion");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wsp","http://schemas.xmlsoap.org/ws/2002/12/policy");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wsu","http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wsa","http://schemas.xmlsoap.org/ws/2004/03/addressing");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wssc","http://schemas.xmlsoap.org/ws/2004/04/sc");
            envelope.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wst","http://schemas.xmlsoap.org/ws/2004/04/trust");

            // <Header>
            Element header = ticket.createElement("Header");

            // <ps:AuthInfo>
            Element psAuthInfo = ticket.createElement("ps:AuthInfo");
            psAuthInfo.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:ps", "http://schemas.microsoft.com/Passport/SoapServices/PPCRL");
            psAuthInfo.setAttribute("Id", "PPAuthInfo");
            // <ps:HostingApp>
            Element psHostingApp = ticket.createElement("ps:HostingApp");
            psHostingApp.setTextContent("{7108E71A-9926-4FCB-BCC9-9A9D3F32E423}");
            psAuthInfo.appendChild(psHostingApp);
            // <ps:BinaryVersion>
            Element psBinaryVersion = ticket.createElement("ps:BinaryVersion");
            psBinaryVersion.setTextContent("4");
            psAuthInfo.appendChild(psBinaryVersion);
            // <ps:UIVersion>
            Element psUIVersion = ticket.createElement("ps:UIVersion");
            psUIVersion.setTextContent("1");
            psAuthInfo.appendChild(psUIVersion);
            // <ps:Cookies>
            Element psCookies = ticket.createElement("ps:Cookies");
            psAuthInfo.appendChild(psCookies);
            // <ps:RequestParams>
            Element psRequestParams= ticket.createElement("ps:RequestParams");
            psRequestParams.setTextContent("AQAAAAIAAABsYwQAAAAxMDMz");
            psAuthInfo.appendChild(psRequestParams);
            // Append <ps:AuthInfo>
            header.appendChild(psAuthInfo);

            // <wsse:Security>
            Element wsseSecurity = ticket.createElement("wsse:Security");
            // <wsse:UsernameToken>
            Element wsseUsernameToken = ticket.createElement("wsse:UsernameToken");
            wsseUsernameToken.setAttribute("Id", "User");
            // <wsse:Username>
            Element wsseUsername = ticket.createElement("wsse:Username");
            wsseUsername.setTextContent(account);
            wsseUsernameToken.appendChild(wsseUsername);
            // <wsse:Password>
            Element wssePassword = ticket.createElement("wsse:Password");
            wssePassword.setTextContent(password);
            wsseUsernameToken.appendChild(wssePassword);
            // Append <wsse:Security>
            wsseSecurity.appendChild(wsseUsernameToken);
            header.appendChild(wsseUsernameToken);
            envelope.appendChild(header);
            // </Header>
            // <Body>
            Element body = ticket.createElement("Body");
            // <ps:RequestMultipleSecurityTokens>
            Element psRequestMultipleSecurityTokens = ticket.createElement("ps:RequestMultipleSecurityTokens");
            psRequestMultipleSecurityTokens.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:ps", "http://schemas.microsoft.com/Passport/SoapServices/PPCRL");
            psRequestMultipleSecurityTokens.setAttribute("Id", "RSTS");
            // <wst:RequestSecurityToken>
            Element wstRequestSecurityToken0 = ticket.createElement("wst:RequestSecurityToken");
            wstRequestSecurityToken0.setAttribute("Id", "RST0");
            Element wstRequestType0 = ticket.createElement("wst:RequestType");
            wstRequestType0.setTextContent("http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue");
            wstRequestSecurityToken0.appendChild(wstRequestType0);

            // </wst:RequestSecurityToken>
            envelope.appendChild(body);
            // </Body>

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            ticket.appendChild(envelope);

            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(ticket);
            transformer.transform(source, result);

            String ticketAsString = sw.toString();

            Log.i(TAG, "Created Ticket.");
            Log.v(TAG, ticketAsString);

        } catch (Exception ex) {
            Log.e(TAG, "Could not create ticket: " + ex.getMessage());
        }**/
    }

    /**
     * Gets and returns the built ticket.
     * @return The ticket built for authentication, used to pass into the
     * authentication.
     */
    public String getTicket() {
        return xml;
    }

}

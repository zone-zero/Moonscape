package com.outlook.paragonazure.moonscape.msn.impl;

import android.util.Log;

import com.outlook.paragonazure.moonscape.msn.MSNResponder;
import com.outlook.paragonazure.moonscape.msn.MsnCommand;
import com.outlook.paragonazure.moonscape.msn.net.MessengerAuth;
import com.outlook.paragonazure.moonscape.msn.security.TicketBuilder;
import com.outlook.paragonazure.moonscape.msn.status.ResponseLogin;

/**
 * Created by paragonazure on 24/12/2017.
 */

public class MessengerResponder implements MSNResponder {


    private static final String TAG = "MessengerResponder";

    public MessengerClient client;

    public MessengerResponder(MessengerClient client) {
        this.client = client;
    }

    @Override
    public void handle(MsnCommand command) {
        if (command.getCommand().equals("USR")) {
            user(command);
        }
    }

    public void user(MsnCommand command) {
        if (command.getArguments()[3].equals("S")) {
            String policy = command.getArguments()[4];
            Log.d(TAG, "Policy given: " + policy);

            String nonce = command.getArguments()[5];
            Log.d(TAG, "Nonce given: " + nonce);

            TicketBuilder builder = new TicketBuilder(command.getArguments()[2], nonce, client.getAccount(), client.getPassword());
            client.setTicket(builder.getTicket());
            client.setSoapUrl("https://m1.escargot.log1p.xyz/RST.srf");

            Log.d(TAG, "clientTicket == " + client.getTicket());
            Log.d(TAG, "     soapUrl == " + client.getSoapUrl());

            MessengerAuth auth = new MessengerAuth(client);
        }
    }
}

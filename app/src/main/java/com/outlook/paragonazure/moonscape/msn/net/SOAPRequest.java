package com.outlook.paragonazure.moonscape.msn.net;

import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by paragonazure on 24/12/2017.
 */

public class SOAPRequest {

    private static String TAG = "SOAPRequest";
    private static final MediaType SOAP_MEDIA_TYPE = MediaType.parse("application/xml");

    private final OkHttpClient client = new OkHttpClient();
    private String message;
    private Request request;
    private Response response;
    private String url;

    public SOAPRequest(String url, String request) {
        RequestBody body = RequestBody.create(SOAP_MEDIA_TYPE, request);

        this.url    = url;
        this.request = new Request.Builder().url(url).method("POST", RequestBody.create(null, new byte[0])).post(body).addHeader("Content-Type", "application/xml").addHeader("cache-control", "no-cache").build();

        client.newCall(this.request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Failed to create request: " + e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                message = response.body().string();
                SOAPRequest.this.response = response;
                Log.d(TAG, "Received response: " + message);
            }
        });
    }

    public Document getResponse() {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return documentBuilder.parse(new ByteArrayInputStream(message.getBytes()));
        } catch (ParserConfigurationException ex) {
            Log.e(TAG, "Error occurred creating parse configuration: " + ex.getMessage());
        } catch (IOException ex) {
            Log.e(TAG, "Error occurred getting document bytes: " + ex.getMessage());
        } catch (SAXException ex) {
            Log.e(TAG, "Error occurred parsing document: " + ex.getMessage());
        }

        return null;
    }

}

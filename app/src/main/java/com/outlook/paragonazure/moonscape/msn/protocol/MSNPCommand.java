package com.outlook.paragonazure.moonscape.msn.protocol;

/**
 * Created by paragonazure on 24/12/2017.
 */

public enum MSNPCommand {
    ANS,
    CHG,
    CVR,
    MSG,
    OUT,
    SYN,
    USR,
    VER,
    XFR
}
